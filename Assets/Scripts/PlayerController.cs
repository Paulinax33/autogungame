using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 5f; // speed of movement of the player
    public Transform target; // Aim of the movement

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        // Keystroke operation
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, moveVertical);
        rb.velocity = movement * speed;

        // Calculating directional vector from player to target
        Vector3 direction = (target.position - transform.position).normalized;
        Vector3 obojetnie = transform.TransformDirection(Vector3.forward);


        // Moving the player towards the target
        //rb.velocity = direction * speed;

    }

    // Function to calculate the distance between two objects
    float DistanceBetweenObjects(GameObject obj1, GameObject obj2)
    {
        Vector3 obj1Pos = obj1.transform.position;
        Vector3 obj2Pos = obj2.transform.position;
        float distance = Vector3.Distance(obj1Pos, obj2Pos);
        return distance;
    }

    // Function to calculate the scalar product of two vectors
    float DotProduct(Vector3 v, Vector3 w)
    {
        float dotProduct = v.x * w.x + v.y * w.y;
        return dotProduct;
    }
    // Function to calculate the angle between two vectors
    float AngleBetweenVectors(Vector3 v, Vector3 w)
    {
        float dotProduct = DotProduct(v, w);
        float magnitudeV = v.magnitude;
        float magnitudeW = w.magnitude;
        float angle = Mathf.Acos(dotProduct / (magnitudeV * magnitudeW)); // In radians
        angle = angle * Mathf.Rad2Deg; // Conversion to degrees
        return angle;
    }
    // Function to calculate the product of two vectors
    Vector3 CrossProduct(Vector3 v, Vector3 w)
    {
        float x = v.y * w.z - v.z * w.y;
        float y = v.z * w.x - v.x * w.z;
        float z = v.x * w.y - v.y * w.x;
        return new Vector3(x, y, z);
    }

}
